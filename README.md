# Petecamático

A project for the automatic counting of hits during a peteca game.

## Resources

Bibliotecas para trabajar con audio:

* https://github.com/RustAudio/cpal
* https://github.com/ruuda/hound

Info sobre el formato wav:

* https://en.wikipedia.org/wiki/WAV

Algo sobre cómo hacer reconocimiento de patrones en audio con python

* https://medium.com/@almeidneto/sound-pattern-recognition-with-python-9aff69edce5d
